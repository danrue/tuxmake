# Contributing to tuxmake

## Development dependencies

The packages needed to develop tuxmake are listed in both `requirements-dev.txt`
and `.gitlab-ci.yml`

## Running the tests

To run the tests, just run `make`: it will run the unit tests first, then the
coding style checks, then the integration tests. Please make sure all the tests
pass before submitting patches.
